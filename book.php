<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <style type="text/css">
            table{
                width: 800px;
                margin: auto;
                text-align: center;
            }
            tr {
                border: 1px solid;
            }
            th {
                border: 1px solid;
            }
            td {
                border: 1px solid;
            }
            h1{
                text-align: center;
                color: red;
            }
            #button{
                margin: 2px;
                margin-right: 10px;
                float: right;
            }
        </style>
    </head>
    <?php
	$con=mysqli_connect("localhost","root","","crud");
    if (mysqli_connect_errno()){
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
    $sql="SELECT * FROM bookmanv1";
    $result=mysqli_query($con,$sql);
    while ($row=mysqli_fetch_assoc($result)){
        $data[] = $row;
};
?>
    <body>
        <table id="datatable" style="border: 1px solid">
            <h1>Quản lý sách</h1>
            <thead>
                <tr role="row">
                    <th>Id</th>
                    <th>Title</th>
                    <th>Authors</th>
                    <th>Publisher</th>
                    <th>Year</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="8">
                        <a href="add.php"><button id="button">Thêm sách</button></a>
                    </td>
                </tr>
            </tfoot>
            <?php $i = 1;
                    foreach ($data as $value){ ?>
                <tr role="row">
                        <td><?php echo $i ?></td>
                        <td><?php echo $value ['bm_Title']?></td>
                        <td><?php echo $value ['bm_Authors']?></td>
                        <td><?php echo $value ['bm_Publisher']?></td>
                        <td><?php echo $value ['bm_Year']?></td>
                        <td><a href="./edit.php?id=<?php echo $value['bm_Id']?>">Edit</a></td>
                        <td><a href="./delete.php?id=<?php echo $value['bm_Id']?>"> Delete</a></td> 
                    </tr>
                    <?php $i++;} ?>
                </tbody>
        </table>
    </body>
    
 
</html>