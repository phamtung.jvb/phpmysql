<?php
    $data = $_POST;
    $errors = [];
    //Validate Title
    if (!is_string($data['Title']) || strlen($data['Title']) < 5 
        || strlen($data['Title']) > 55) {
         $errors['Title'] = $data['Title'] . "Title không hợp lệ!";
    }
    //Validate Authors
    if (!is_string($data['Authors']) || strlen($data['Authors']) < 2 
        || strlen($data['Authors']) > 55) {
        $errors['Authors'] = "Authors không hợp lệ!";
    }
    //Validate Publisher
    if (!is_string($data['Publisher']) || strlen($data['Publisher']) < 5
        || strlen($data['Publisher']) > 40) {
        $errors['Publisher'] = "Publisher không hợp lệ!";
    }
    //Validate Year
    if (!is_numeric($data['Year']) || $data['Year'] < 0 || $data['Year'] > 1000000) {
        $errors['Year'] = "Year không hợp lệ!";
    }
    if (count($errors) > 0) {
        $err_str = '<ul>';
        foreach ($errors as $err) {
            $err_str .= '<li>'.$err.'</li>';
        }   
        $err_str .= '</ul>';
        echo  $err_str;
    }else{
        try {
            $con = mysqli_connect('localhost', 'root', '', 'crud'); 
            $sql = "UPDATE bookmanv1 SET bm_Title='".$data['Title']."',bm_Authors= '".$data['Authors']."',bm_Publisher='".$data['Publisher']."',bm_Year='".$data['Year']."' WHERE bm_Id =".$data['id'];
            $result = mysqli_query($con,$sql);
            if ($result) {
                header ("Location:/mysql/book.php");
            }else{
                echo "<h1>Có lỗi xảy ra Click vào 
                <a href='book.php'>đây</a> để về trang danh sách</h1>";
            }
        } catch (Exception $e) {
        }
        
    }
?>